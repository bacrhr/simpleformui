/*************************************************
**  jQuery simpleFormUI version 0.3.1
**  Copyright Laust Deleuran, licensed GNU GPL v3
**  http://dev.ljd.dk/simpleformui
**************************************************/
(function($) {
  $.fn.disableTextSelect=function() {
    return this.each(function() {
      $(this).css({
        'MozUserSelect': 'none'
      }).select(function() {
        return false;
      }).mousedown(function() {
        return false;
      });
    });
  };
  $.fn.enableTextSelect=function() {
    return this.each(function() {
      $(this).css({
        'MozUserSelect': ''
      }).select(function() {
        return true;
      }).mousedown(function() {
        return true;
      });
    });
  };
  $.fn.simpleFormUI=function(options) {

    // Settings
    var settings={
      cssClass: 'simpleFormUI',
      inheritClasses: true,
      inlineStyles: true,
      keyboardSupport: true,
      hideStyles: { 'visibility': 'hidden','position': 'absolute','left': '-9999em','outline': 'none none'} // {'position':'absolute','left':'-20000px'} // { 'display':'none' } IE doesnt seem to change state on none-displayed elements
    };
    if(options) {
      $.extend(settings,options);
    }
    var cssClassSelector=settings.cssClass?'.'+settings.cssClass:'';

    var sfui={
      selectOption: function(link,selector,$active,event) {
        if($(link).attr('id')) {
          var $link=$(link);
          var $origin=$link.closest(selector+'.select');
          var val=$link.attr('id').split('select-')[1];
          var $option=$origin.find('option[value="'+val+'"]');
          event = typeof event === 'undefined' ? true  : event;
          $origin.find('li').removeClass('selected');
          $link.addClass('selected');

          $origin.find('option').removeAttr('selected');
          $option.attr('selected','selected');
          if (event) $option.parent().trigger('change');
          $active.text($option.text()).removeAttr('class').attr('class',$link.attr('data-cssclass'));
        }
      }
    }
    // Loop and return object collection
    return this.each(function() {

      // Select boxes
      if($(this).is('select')) {
        var $this=$(this);
        if(!$this.parent().hasClass(settings.cssClass)) {
            if(settings.inlineStyles) { $(this).css(settings.hideStyles); }
            var containerStyle=settings.inlineStyles?' style="position:relative;"':'';
            $(this).wrap('<div class="'+settings.cssClass+' select"'+containerStyle+'></div>');
            var $container=$(this).parent(cssClassSelector+'.select');
            if ($(this).attr('class') != '' && settings.inheritClasses) {
              $container.addClass($(this).attr('class'));
              $container.attr('data-cssclass',$(this).attr('class'));
            }
            var overlayStyle=settings.inlineStyles?' style="height:100%;left:0;position:fixed;top:0;width:100%;z-index:8000;display:none;"':'';
            if($('#simpleFormUIOverlay').length==0) { $('body').append('<div id="simpleFormUIOverlay"'+overlayStyle+'></div>'); }
            $container.append('<div></div>');
            var ulStyle=settings.inlineStyles?' style="display:none;position:absolute;top:110%;left:0;z-index:9000;margin:0;"':'';
            $container.append('<ul'+ulStyle+'></ul>');
            var $list=$(this).parent(cssClassSelector+'.select').find('ul');
            var $box=$(this).parent(cssClassSelector+'.select').find('div');
            var boxStyle=settings.inlineStyles?' style="display:block;overflow:hidden;"':'';
            $box.append('<a href="javascript:void(0);"'+boxStyle+'>Default</a>');
            var $active=$(this).parent(cssClassSelector+'.select').find('div a');
            var $overlay=$('#simpleFormUIOverlay');
            $box.click(function() {
              var $thisList=$box.parent(cssClassSelector+'.select').find('ul');
              if($thisList.css('display')=='block') {
                $overlay.css('display','none');
                $thisList.css('display','none');
                $container.css('z-index',1);
              } else {
                $overlay.css('display','block');
                $thisList.css('display','block').css({'top':'110%','bottom':'auto'});
                if($thisList.offset().top+$thisList.outerHeight() > $(window).height()+$(window).scrollTop()) {
                  $thisList.css({'top':'auto','bottom':'110%'});   
                } 
                $container.css('z-index',10000);
              }
            });
            $overlay.click(function(e) {
              e.stopPropagation();
              $overlay.css('display','none');
              $container.css('z-index',1);
              $(cssClassSelector+'.select ul').css('display','none');
            });
            $box.keydown(function(e) {
              if(e.keyCode==38||e.keyCode==40) {
                e.preventDefault();
                if($(cssClassSelector+'.select li.selected')[0]) {
                  if(e.keyCode==38) { // Up arrow
                    sfui.selectOption($(this).parent(cssClassSelector+'.select').find('li.selected').prev('li'),cssClassSelector,$active);
                  } else if(e.keyCode==40) { // Down arrow
                    sfui.selectOption($(this).parent(cssClassSelector+'.select').find('li.selected').next('li'),cssClassSelector,$active);
                  }
                } else {
                  $(cssClassSelector+'.select li')[0].click();
                }
              }
            });
        } else {
            var wrapped_div = $(this).parent(cssClassSelector+'.select');
            var $list=wrapped_div.find('ul');
            var $box=wrapped_div.find('div');
            var $active=wrapped_div.find('div a');
            $list.find('li').remove();
        }
        $(this).find('option').each(function(i) {
          var listStyles=settings.inlineStyles?' style="list-style:none; margin:0;"':'';
          var $item=$('<li'+listStyles+'><a href="#">'+$(this).text()+'</a></li>');
          if ($(this).attr('class') != '') {
            $item.attr('class',$(this).attr('class'));
            $item.attr('data-cssclass',$(this).attr('class'));
          }
          $item.attr('id','select-'+$(this).attr('value'));
          $item.click(function(e) {
            e.preventDefault();
            sfui.selectOption(this,cssClassSelector,$active);
            $box.click();
          });
          $list.append($item);
          if(this.selected) {
            sfui.selectOption($item,cssClassSelector,$active,false);
          }
        });

      // Check boxes
      } else if($(this).is('input[type=checkbox]')) {
        if(settings.inlineStyles) { $(this).css(settings.hideStyles); }
        var containerStyle=settings.inlineStyles?' style="display:block;"':'';
        $(this).wrap('<a href="#" class="'+settings.cssClass+' checkbox"'+containerStyle+'></a>').parent('a').click(function(e) {
          e.stopPropagation();
          e.preventDefault();
          $(this).find('input')[0].checked=$(this).find('input')[0].checked?false:true;
          $(this).removeClass("checked");
          if($(this).find('input')[0].checked) {
            $(this).addClass("checked");
          }
        });
        $(this).change(function() {
          $(this).parent('a').removeClass("checked");
          if(this.checked) {
            $(this).parent('a').addClass("checked");
          }
        });
        $(this).click(function(e) {
          e.stopPropagation();
        });
        if(this.checked) {
          $(this).parent('a').addClass("checked");
        }

      // Radio buttons
      } else if($(this).is('input[type=radio]')) {
        var $this=$(this);
        if(settings.inlineStyles) { $this.css(settings.hideStyles); }
        var containerStyle=settings.inlineStyles?' style="display:block;"':'';
        $this.wrap('<a href="#" class="'+settings.cssClass+' radio"'+containerStyle+'></a>').parent('a').click(function(e) {
          e.stopPropagation();
          e.preventDefault();
          $(this).find('input')[0].checked=true;
          $('input[name="'+$this.attr("name")+'"]').parent(cssClassSelector).removeClass("checked");
          if($(this).find('input')[0].checked) {
            $(this).addClass("checked");
          }
        });
        $this.change(function() {
          $('input[name="'+$this.attr("name")+'"]').parent(cssClassSelector).removeClass("checked");
          if(this.checked) {
            $this.parent('a').addClass("checked");
          }
        });
        $('input[name="'+$this.attr("name")+'"]:checked').parent(cssClassSelector).addClass("checked");

      // Text input elements - Simple fallback for transitional elements
      } else if($(this).is('input[type=text],input[type=email],input[type=url],input[type=password],input[type=search],input[type=tel],input[type=number],input[type=datetime],input[type=date],input[type=month],input[type=week],input[type=time],input[type=datetime-local],input[type=range],input[type=color]')) {
        $(this).wrap('<span class="'+settings.cssClass+' field '+$(this).attr('type')+'"></span>');

      // Textarea
      } else if($(this).is('textarea')) {
        $(this).wrap('<div class="'+settings.cssClass+' textarea"></div>');

      // Buttons
      } else if($(this).is('input[type=button], input[type=submit], button')) {
        $(this).wrap('<span class="'+settings.cssClass+' button"></span>');
      }
    });
  };
})(jQuery);